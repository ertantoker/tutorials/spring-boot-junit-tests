package de.eretantoker.tutorials.customerservice.service;

import de.eretantoker.tutorials.customerservice.exception.EntityAlreadyExistsException;
import de.eretantoker.tutorials.customerservice.exception.EntityNotFoundException;
import de.eretantoker.tutorials.customerservice.model.Customer;
import de.eretantoker.tutorials.customerservice.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    private static final String ALREADY_EXISTS_MESSAGE = "Customer with firstName: %s and lastName: %s is already existing";

    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(String id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Customer not found with id: %s", id)));
    }

    public Customer createCustomer(Customer customer) {
        checkExistingCustomer(customer);

        customerRepository.save(customer);
        return customer;
    }

    private void checkExistingCustomer(Customer customer) {
        customerRepository.findByFirstNameAndLastName(customer.getFirstName(), customer.getLastName())
                .ifPresent(customer1 -> {
                    throw new EntityAlreadyExistsException(String.format(ALREADY_EXISTS_MESSAGE, customer.getFirstName(), customer.getLastName()));
                });

    }

    public void deleteCustomer(String id) {
        customerRepository.deleteById(id);
    }
}
