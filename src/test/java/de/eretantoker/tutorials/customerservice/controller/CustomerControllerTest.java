package de.eretantoker.tutorials.customerservice.controller;

import de.eretantoker.tutorials.customerservice.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class CustomerControllerTest {

    private CustomerController sut;

    private CustomerService customerService;

    @BeforeEach
    public void setup() {

        customerService = Mockito.mock(CustomerService.class);

        sut = new CustomerController(customerService);
    }

    @Test
    public void callCustomerService_getCustomers() {
        sut.getCustomers();
        Mockito.verify(customerService).getCustomers();
    }

    @Test
    public void callCustomerService_getCustomerById() {
        String id = "someId";
        sut.getCustomerById(id);
        Mockito.verify(customerService).getCustomerById(Mockito.eq(id));
    }

}