package de.eretantoker.tutorials.customerservice.service;

import de.eretantoker.tutorials.customerservice.exception.EntityNotFoundException;
import de.eretantoker.tutorials.customerservice.model.Customer;
import de.eretantoker.tutorials.customerservice.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

class CustomerServiceText {

    private CustomerService sut;

    private CustomerRepository customerRepository;

    @BeforeEach
    public void setup() {
        customerRepository = Mockito.mock(CustomerRepository.class);
        sut = new CustomerService(customerRepository);
    }

    @Test
    public void callFindById_shouldReturnCustomer() {
        String id = "someId";

        Customer customer = new Customer();
        customer.setFirstName("someFirstName");

        when(customerRepository.findById(eq(id))).thenReturn(Optional.of(customer));

        Customer result = sut.getCustomerById(id);

        assertNotNull(result);
        assertEquals("someFirstName", result.getFirstName());

        verify(customerRepository).findById(eq(id));
    }

    @Test
    public void callFindById_shouldThrowEntityNotFoundException() {
        String id = "someId";

        Customer customer = new Customer();
        customer.setFirstName("someFirstName");

        when(customerRepository.findById(eq(id))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            sut.getCustomerById(id);
        });
    }

}
